import {API_ADDRESS} from './Config.js'

const fetchInfo = async (query) => {
    try{
        const request = await fetch(API_ADDRESS + query);
        return await request.json();
    }
    catch(err){
        alert(`${err} error occured with fetching API info`);
    }
}

const getProducts = async (sort = null) => await fetchInfo(`products${sort ? `?sort=${sort}` : '' }`);
const getCategories = async () => await fetchInfo('products/categories');

export {getProducts, getCategories};
