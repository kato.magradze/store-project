import {getProducts, getCategories} from './API.js'
import {makeProduct} from './Product.js'

let catalogHTML = document.getElementsByClassName('main__item--catalog')[0];

const fillCatalog = (sort = null) => {
    getProducts(sort).then(allProducts => {
        createProductHTML(allProducts);
    });
}

const createProductHTML = (objectsArr) => {
    let productsHTML = '';
    for(let product of objectsArr) {
        productsHTML += makeProduct(product);
    }
    catalogHTML.innerHTML = productsHTML;
}

fillCatalog();

//SORT
// const sort = document.getElementById('sort');
// sort.addEventListener('change', () => fillCatalog(sort.value));

//========================================================================

//ALTERNATIVE SORT

const sortAscending = (objectsArr) => {
    objectsArr.sort((a,b) => a["price"] - b["price"]);
    createProductHTML(objectsArr);
}

const sortDescending = (objectsArr) => {
    objectsArr.sort((a,b) => b["price"] - a["price"]);
    createProductHTML(objectsArr);
}

const sortAlphabetically = (objectsArr) => {
    objectsArr.sort((a,b) => a.title.localeCompare(b.title));
    createProductHTML(objectsArr);
}

const sortProducts = (sortVal, objectsArr) => {
    if(sortVal == "asc") {
        sortAscending(objectsArr);
    }
    else if(sortVal == "desc") {
        sortDescending(objectsArr);
    }
    else if(sortVal == "alphabetic") {
        sortAlphabetically(objectsArr);
    }
    else {
        createProductHTML(objectsArr);
    }
}

const sort = document.getElementById('sort');
let sortValue = '';

sort.addEventListener('change', () => {
    sortValue = sort.value;
    getProducts().then(allProducts => {
        let filteredProducts = allProducts.filter(product => product.title.toLowerCase().includes(searchQuery.value.toLowerCase()));
        sortProducts(sortValue, filteredProducts);
    })
    
});

//SEARCH

const searchQuery = document.getElementById('searchQuery');
const searchButton = document.getElementById('searchButton');

searchButton.addEventListener('click', () => {
    getProducts().then(allProducts => {
        let filteredProducts = allProducts.filter(product => product.title.toLowerCase().includes(searchQuery.value.toLowerCase()));
        sortProducts(sortValue, filteredProducts);
    })
})